﻿#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <time.h>


int main()
{
    SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	srand(time(0));
	int arr[14];
	int max=0, maxi, min = 0, mini, i, b;
	
	printf("Початковий масив\n\n");	
	for (i = 0; i < 14; i++)
	{
		arr[i] = -100 + rand() % (100 - (-100) + 1);
		printf("%d ", arr[i]);
	}
	for (i = 0; i < 14; i++)
	{
		if (arr[i] >= max)
		{
			max = arr[i];
			maxi = i;
		}
		if (arr[i] <= min)
		{
			min = arr[i];
			mini = i;
		}
	}
	printf("\n\nМаксимальне ->%d ", max); printf("мінімальне ->%d\n", min);
	b = arr[13];
	arr[13] = arr[maxi];
	arr[maxi] = b;
	b = arr[0];
	arr[0] = arr[mini];
	arr[mini] = b;
	printf("Масив після заданих операцій\n\n");
	for (i = 0; i < 14; i++)
		printf("%d ", arr[i]);
	printf("\n\n");

	return 0;

}

